import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './components/table/table.component';
import { MoviesRoutingModule } from '../features/movies/movies-routing.module';
import { UsersRoutingModule } from '../features/users/users-routing.module';

const PUBLIC_COMPONENTS: any[] = [TableComponent];
const PUBLIC_DIRECTIVES: any[] = [];
const PUBLIC_PIPES: any[] = [];

@NgModule({
	declarations: [
		...PUBLIC_COMPONENTS,
		...PUBLIC_DIRECTIVES,
		...PUBLIC_PIPES,
	],
	imports: [
		CommonModule,
		MoviesRoutingModule,
		UsersRoutingModule
	],
	exports: [
		CommonModule,
		...PUBLIC_COMPONENTS,
		...PUBLIC_DIRECTIVES,
		...PUBLIC_PIPES,
	],
})
export class SharedModule { }
